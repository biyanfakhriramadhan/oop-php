<?php

class Animal{
    public $name = "";
    public $legs = "4";
    public $cold_blooded = "No";
    public function __construct($name){
    return $this->name = $name;
    }
    public function getName(){
        return "Name: " . $this->name . "<br>";
    }
    public function getLegs(){
        return "Legs: " . $this->legs . "<br>";
    }
    public function getCold_blooded(){
        return "Cold Blooded: " . $this->cold_blooded . "<br>";
    }
}


?>