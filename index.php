<?php

require_once "animal.php";
require_once "ape.php";
require_once "frog.php";

$sheep = new Animal("shaun");

echo $sheep->getName();
echo $sheep->getLegs();
echo $sheep->getCold_blooded();

echo "<br>";

$kodok = new Frog("buduk");

echo $kodok->getName();
echo $kodok->getLegs();
echo $kodok->getCold_blooded();
echo $kodok->jump();

echo "<br>";

$sungokong = new Ape("sungokong");

echo $sungokong->getName();
echo $sungokong->getLegs();
echo $sungokong->getCold_blooded();
echo $sungokong->yell();

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())